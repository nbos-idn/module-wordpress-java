package io.nbos.capi.modules.wordpress.v0.models.blog;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by devenv on 3/6/17.
 */
@RealmClass
public class RealmString extends RealmObject {
    private String value;


    public RealmString() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public RealmString(String value) {
        this.value = value;
    }

}
