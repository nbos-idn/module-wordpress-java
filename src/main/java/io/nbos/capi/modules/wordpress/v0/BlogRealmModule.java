package io.nbos.capi.modules.wordpress.v0;

import io.nbos.capi.modules.wordpress.v0.models.blog.BlogPost;
import io.realm.annotations.RealmModule;

/**
 * Created by devenv on 12/29/16.
 */
@RealmModule(library = true, classes = {BlogPost.class})
public class BlogRealmModule {
}
