package io.nbos.capi.modules.wordpress.v0;

import io.nbos.capi.modules.wordpress.v0.models.blog.BlogCategory;
import io.realm.annotations.RealmModule;

/**
 * Created by devenv on 12/29/16.
 */

@RealmModule(library = true, classes = {BlogCategory.class})
public class WordpressCategoryModule {
}
