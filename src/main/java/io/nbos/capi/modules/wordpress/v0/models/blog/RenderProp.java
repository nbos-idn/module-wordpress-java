package io.nbos.capi.modules.wordpress.v0.models.blog;

import io.realm.RealmObject;

/**
 * Created by ashkumar on 5/16/2016.
 */
public class RenderProp extends RealmObject{


    private String rendered;

    public String getRendered() {
        return rendered;
    }

    public void setRendered(String rendered) {
        this.rendered = rendered;
    }

}
