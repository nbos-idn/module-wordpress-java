package io.nbos.capi.modules.wordpress.v0.models.blog;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by devenv on 3/6/17.
 */
@RealmClass
public class RealmBoolean extends RealmObject {
    private Boolean val;

    public RealmBoolean() {
    }

    public Boolean getVal() {
        return val;
    }

    public void setVal(Boolean val) {
        this.val = val;
    }

    public RealmBoolean(Boolean val) {
        this.val = val;
    }
}
