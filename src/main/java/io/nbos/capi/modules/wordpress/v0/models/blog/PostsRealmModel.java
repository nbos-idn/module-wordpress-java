package io.nbos.capi.modules.wordpress.v0.models.blog;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by devenv on 3/8/17.
 */

public class PostsRealmModel extends RealmObject implements Serializable{
    private String thumbnail;
    private String medium;
    private String medium_large;
    private String ios_thumbnail;

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getMedium_large() {
        return medium_large;
    }

    public void setMedium_large(String medium_large) {
        this.medium_large = medium_large;
    }

    public String getIos_thumbnail() {
        return ios_thumbnail;
    }

    public void setIos_thumbnail(String ios_thumbnail) {
        this.ios_thumbnail = ios_thumbnail;
    }

}
