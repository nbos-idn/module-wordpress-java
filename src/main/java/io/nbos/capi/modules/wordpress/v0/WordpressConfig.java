package io.nbos.capi.modules.wordpress.v0;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by devenv on 12/29/16.
 */

public class WordpressConfig {
    private final RealmConfiguration realmConfig;
    private Realm realm;

    public WordpressConfig() {
        realmConfig = new RealmConfiguration.Builder()     // The app is responsible for calling `Realm.init(Context)`
                .name("library.wordpress.realm")                 // So always use a unique name
                .modules(new WordpressCategoryModule())           // Always use explicit modules in library projects
                .build();

        // Reset Realm
        Realm.deleteRealm(realmConfig);
    }

    public void open() {
        // Don't use Realm.setDefaultInstance() in library projects. It is unsafe as app developers can override the
        // default configuration. So always use explicit configurations in library projects.
        realm = Realm.getInstance(realmConfig);
    }



    public void close() {
        realm.close();
    }

}
