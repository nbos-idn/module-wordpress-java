package io.nbos.capi.modules.wordpress.v0.models.blog;


import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by ashkumar on 5/16/2016.
 */
public class BlogFeatureImage extends RealmObject {


    private RealmList<RealmString> thumbnail;

    private RealmList<RealmString> medium;

    private RealmList<RealmString> medium_large;

    public RealmList<RealmString> getThumbnail() {
        return thumbnail;
    }

    public RealmList<RealmString> getMedium_large() {
        return medium_large;
    }

    public RealmList<RealmString> getMedium() {
        return medium;
    }

    public void setAllValues(RealmString allValues) {
        this.allValues = allValues;
    }

    public RealmString getAllValues() {
        return allValues;
    }

    private RealmString allValues;


}
