package io.nbos.capi.modules.wordpress.v0.utils;

/**
 * Created by devenv on 3/22/17.
 */

public class WPConstants {
    public static final String blogCategoriesURL = "/wp-json/wp/v2/categories";
    public static final String blogPostListURL = "/wp-json/wp/v2/posts";
    public static final String singleblogPostURL = "/wp-json/wp/v2/posts/{id}";


}
