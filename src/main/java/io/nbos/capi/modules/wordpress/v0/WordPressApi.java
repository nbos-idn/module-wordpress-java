package io.nbos.capi.modules.wordpress.v0;


import io.nbos.capi.api.v0.support.IdnCallback;
import io.nbos.capi.api.v0.support.NetworkApi;
import io.nbos.capi.modules.wordpress.v0.models.blog.BlogCategory;

import io.realm.RealmList;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vivekkiran on 7/29/16.
 */
//@IdsModule(name = "wordpress", type = WordPressApi.class)
public class WordPressApi extends NetworkApi {
    public WordPressApi() {
        super();
        setModuleName("wordpress");
        setRemoteApiClass(WPRemoteApi.class);
    }

    public RealmList<BlogCategory> getCategories(int categoryId, String exclude, final IdnCallback<RealmList<BlogCategory>> callback) {
        WPRemoteApi wpRemoteApi = getRemoteApi();

        Call<RealmList<BlogCategory>> call = wpRemoteApi.getCategories(categoryId,20,exclude);

        RealmList<BlogCategory> blogCategory = null;
        call.enqueue(new Callback<RealmList<BlogCategory>>() {
            @Override
            public void onResponse(Call<RealmList<BlogCategory>> call, Response<RealmList<BlogCategory>> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<RealmList<BlogCategory>> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return blogCategory;
    }

    public ResponseBody getPosts(int categoryFilter, int pageNumber, int perPage, final IdnCallback<ResponseBody> callback) {
        WPRemoteApi wpRemoteApi = getRemoteApi();

        Call<ResponseBody> call = wpRemoteApi.getBlogs(categoryFilter, pageNumber, perPage);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return null;
    }

    public ResponseBody getSinglePost(String postID, final IdnCallback<ResponseBody> callback){

        WPRemoteApi wpRemoteApi = getRemoteApi();

        Call<ResponseBody> call = wpRemoteApi.getSingleBlog(postID);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    callback.onResponse(response);
                } else {
                    System.out.println(response);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callback.onFailure(t);
            }
        });
        return null;

    }



}
