package io.nbos.capi.modules.wordpress.v0;

import io.nbos.capi.modules.wordpress.v0.models.blog.BlogCategory;

import io.nbos.capi.modules.wordpress.v0.utils.WPConstants;
import io.realm.RealmList;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by vivekkiran on 7/29/16.
 */

public interface WPRemoteApi {

    @GET(WPConstants.blogCategoriesURL)
    Call<RealmList<BlogCategory>> getCategories(@Query("parent") int parent, @Query("per_page")int perPage,@Query("exclude") String exclude);

    @GET(WPConstants.blogPostListURL)
    Call<ResponseBody> getBlogs(@Query("filter[cat]") int categoryId, @Query("page") int page, @Query("per_page") int perPage);

    @GET(WPConstants.singleblogPostURL)
    Call<ResponseBody> getSingleBlog(@Path("id") String postID);

}
