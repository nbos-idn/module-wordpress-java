package io.nbos.capi.modules.wordpress.v0.models.blog;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by devenv on 3/6/17.
 */
@RealmClass
public class RealmInt extends RealmObject {
    private int val;

    public RealmInt() {
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public RealmInt(int val) {
        this.val = val;
    }
}
