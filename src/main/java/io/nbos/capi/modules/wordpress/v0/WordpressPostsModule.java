package io.nbos.capi.modules.wordpress.v0;

import io.nbos.capi.modules.wordpress.v0.models.blog.BlogFeatureImage;
import io.nbos.capi.modules.wordpress.v0.models.blog.BlogPost;
import io.nbos.capi.modules.wordpress.v0.models.blog.PostsRealmModel;
import io.nbos.capi.modules.wordpress.v0.models.blog.RealmInt;
import io.nbos.capi.modules.wordpress.v0.models.blog.RealmString;
import io.nbos.capi.modules.wordpress.v0.models.blog.RenderProp;
import io.realm.annotations.RealmModule;

/**
 * Created by devenv on 12/29/16.
 */

@RealmModule(library = true, classes = {BlogPost.class, PostsRealmModel.class, BlogFeatureImage.class,RealmInt.class, RealmString.class, RenderProp.class})
public class WordpressPostsModule {
}
