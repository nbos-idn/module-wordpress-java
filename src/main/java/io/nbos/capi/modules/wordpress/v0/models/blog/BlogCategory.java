package io.nbos.capi.modules.wordpress.v0.models.blog;


import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by ashkumar on 5/15/2016.
 */

@Parcel(analyze = {BlogCategory.class})
public class BlogCategory extends RealmObject {

    @PrimaryKey
    int id;


    int position;

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    int parent;

    Long count;


    String description;

    String link;


    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
