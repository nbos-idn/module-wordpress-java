package io.nbos.capi.modules.wordpress.v0.models.blog;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ashkumar on 5/15/2016.
 */
public class BlogPost extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;

//    public int post_id;

    private String date;
    private String date_gmt;
    private RenderProp guid;
    private String modified;
    private String modified_gmt;
    private String slug;
    private String type;
    private String link;
    private PostsRealmModel feature_image;
    private RealmList<RealmString> ask_astrologer;
    private RenderProp title;
    private RenderProp content;
    private RenderProp excerpt;
    private Long author;
    private Long featured_media;
    private String comment_status;
    private String ping_status;
    private Boolean sticky;
    private String format;
    private RealmList<RealmInt> categories;
    private RealmList<RealmInt> tags;
    private String post_title;


    public void setId(int id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDate_gmt(String date_gmt) {
        this.date_gmt = date_gmt;
    }

    public void setGuid(RenderProp guid) {
        this.guid = guid;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public void setModified_gmt(String modified_gmt) {
        this.modified_gmt = modified_gmt;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setAsk_astrologer(RealmList<RealmString> ask_astrologer) {
        this.ask_astrologer = ask_astrologer;
    }

    public void setTitle(RenderProp title) {
        this.title = title;
    }

    public void setContent(RenderProp content) {
        this.content = content;
    }

    public void setExcerpt(RenderProp excerpt) {
        this.excerpt = excerpt;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public void setFeatured_media(Long featured_media) {
        this.featured_media = featured_media;
    }

    public void setComment_status(String comment_status) {
        this.comment_status = comment_status;
    }

    public void setPing_status(String ping_status) {
        this.ping_status = ping_status;
    }

    public void setSticky(Boolean sticky) {
        this.sticky = sticky;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setCategories(RealmList<RealmInt> categories) {
        this.categories = categories;
    }

    public void setTags(RealmList<RealmInt> tags) {
        this.tags = tags;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public void setFeature_image(PostsRealmModel feature_image) {
        this.feature_image = feature_image;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getDate_gmt() {
        return date_gmt;
    }

    public RenderProp getGuid() {
        return guid;
    }

    public String getModified() {
        return modified;
    }

    public String getModified_gmt() {
        return modified_gmt;
    }

    public String getSlug() {
        return slug;
    }

    public String getType() {
        return type;
    }

    public String getLink() {
        return link;
    }

    public PostsRealmModel getFeature_image() {
        return feature_image;
    }

    public RealmList<RealmString> getAsk_astrologer() {
        return ask_astrologer;
    }

    public RenderProp getTitle() {
        return title;
    }

    public RenderProp getContent() {
        return content;
    }

    public RenderProp getExcerpt() {
        return excerpt;
    }

    public Long getAuthor() {
        return author;
    }

    public Long getFeatured_media() {
        return featured_media;
    }

    public String getComment_status() {
        return comment_status;
    }

    public String getPing_status() {
        return ping_status;
    }

    public Boolean getSticky() {
        return sticky;
    }

    public String getFormat() {
        return format;
    }

    public RealmList<RealmInt> getCategories() {
        return categories;
    }

    public RealmList<RealmInt> getTags() {
        return tags;
    }


    public String getPost_title() {
        return post_title;
    }

//    public int getPost_id() {
//        return post_id;
//    }
//
//    public void setPost_id(int post_id) {
//        this.post_id = post_id;
//    }


}
